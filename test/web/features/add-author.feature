Feature: User can add a new Author into list
  As an user
  I want to add a author to author list
  So that I can record and keep track the authors in store

  Scenario: Create a author
    Given an authors list
      | name         |
      | Taylor David |
      | Johny Jack   |
      | Marry        |
    When I add an author with the name "Karik Ho"
    Then there are exactly "4" items in the authors list
    When I add a book list with authorId related author with name "Karik Ho"
      | title             | price | soldTotal |
      | HTML5 Canvas      | 6     | 8         |
      | Node.js in action | 8     | 4         |
      | Java Cookbook     | 7     | 2         |
    Then author with name "Karik Ho" has "14" sold books