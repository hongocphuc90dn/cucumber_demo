'use strict';

var async = require('async');
var lodash = require('lodash');
var assert = require('chai').assert;
var logger = require('../../../../server/lib/utils/logger.js');
var Promise = require('bluebird');
module.exports = function() {
    this.World = require('../support/world.js').World;
    
   var author = null;

	this.Given(/^an authors list$/,function(table, done) {
    	var self = this;
    	var authorsList = table.hashes();
		var authorModel = self.app.models.Author;
		var bookModel = self.app.models.Book;

		bookModel.destroyAll(function(err, info) {
			authorModel.destroyAll(function(err, info) {
				async.each(authorsList, function(author, callback) {
					authorModel.create(author, function(err, obj) {
						callback();
					});
				}, function (error) {
					done();
				});
			});
		})

	});
 
    this.When(/^I add an author with the name "([^"]*)"$/, function (name,  done) {
    	var self = this;
    	self.request
	      	.post('/api/Authors')
	      	.send({ 
	      		name: name, 
	      	})
	      	.expect('Content-Type', /json/)
			.expect(200)
	      	.end(function(err, res) {  
	        	done();
	      	});	
    });

    this.Then(/^there are exactly "([^"]*)" items in the authors list$/, function (authorsCount, done) {
	  	var self = this;
    	self.request
	      	.get('/api/Authors/count')
	      	.expect('Content-Type', /json/)
			.expect(200)
	      	.end(function(err, res) {  
	        	assert.isNumber(res.body.count, 'the output should  be a number');
	        	assert.equal(res.body.count, authorsCount,'Authos list should have exactly ' + authorsCount + ' in total');
	        	done();
	      	});
	});


    this.When(/^I add a book list with authorId related author with name "([^"]*)"$/, function (name, table,  done) {
    	var self = this;
    	var booksList = table.hashes();
        
        async.waterfall([
            function(callback){
                self.request
                    .get('/api/Authors/findOne?filter[where][name]='+name)
                    .send()
                    .expect('Content-Type', /json/)
                    .expect(200)
                    .end(function(err, res) {        
                        author = res.body;
                        callback(err, author);
                    });	 
            },
            function(author,callback){
                async.eachSeries(booksList, function(book, subCallback) {
                    self.request
                        .post('/api/Books')
                        .send({
                            title: book["title"], 
                            price: book["price"],
                            soldTotal: book["soldTotal"],
                            authorId: author.id
                        })
                        .expect('Content-Type', /json/)
                        .expect(200)
                        .end(function(err, res) {
                            subCallback();

                        });	
                }, function (error) {
                    callback(error);
                });
            }
         ], function(err){
            assert(lodash.isNull(err));
            done();
        });

    });

    this.Then(/^author with name "([^"]*)" has "([^"]*)" sold books$/, function (name, total, done) {
    	var self = this;
        console.log("author: ", author)
        self.request
            .get('/api/Authors/'+author.id+'/books/sold/total')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function(err, res) {                     
                assert(lodash.isNull(err));
                assert.equal(res.body.total, total,'the total of sold books of this author should be '+total);
                done();
            });	
	});





}