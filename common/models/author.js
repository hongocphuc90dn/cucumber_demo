var lodash = require('lodash');
var app = require('../../server/server.js');

module.exports = function(Author) {

	Author.soldBooksTotal = function(id, cb) {
        var total = 0;
        app.models.Books.find({authorId:id}, function(err,books){
            for(var i=0;i<books.length;i++){
                total = total + books[i].soldTotal;
            }
            cb(err, total);
        });
        
    }

    Author.remoteMethod(
        'soldBooksTotal', 
        {
          accepts: {arg: 'id', type: 'string'},
          http: {verb: 'get', path: '/:id/books/sold/total'},
          returns: {arg: 'total', type: 'number'}
        }
    );
};
